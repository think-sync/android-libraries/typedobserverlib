package com.thinknsync.observerhost;

public abstract class TypedObserverImpl<T> implements TypedObserver<T> {

    @Override
    public void update(ObserverHost<T> self, T object){
        update(object);
    }

    @Override
    public void onOperationError(ObserverHost<T> self, Exception e) {
        e.printStackTrace();
    }
}
