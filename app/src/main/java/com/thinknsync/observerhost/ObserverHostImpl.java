package com.thinknsync.observerhost;

import java.util.ArrayList;
import java.util.List;

public class ObserverHostImpl<T> implements ObserverHost<T> {

    protected List<TypedObserver<T>> observerList = new ArrayList<>();

    @Override
    public synchronized void addToObservers(TypedObserver<T> observer) {
        observerList.add(observer);
    }

    @Override
    public synchronized void addToObservers(List<TypedObserver<T>> observers) {
        observerList.addAll(observers);
    }

    @Override
    public synchronized void removeObserver(TypedObserver<T> observer) {
        observerList.remove(observer);
    }

    @Override
    public void clearObservers() {
        observerList = new ArrayList<>();
    }

    @Override
    public void notifyObservers(T object) {
        for (TypedObserver<T> o : observerList) {
            o.update(this, object);
        }
    }

    @Override
    public void notifyError(Exception e) {
        for (TypedObserver<T> o : observerList) {
            o.onOperationError(this, e);
        }
    }
}
