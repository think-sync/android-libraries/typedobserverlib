package com.thinknsync.observerhost;

public interface TypedObserver<T> {
    void update(T object);
    void update(ObserverHost<T> self, T object);
    void onOperationError(ObserverHost<T> self, Exception e);
}
