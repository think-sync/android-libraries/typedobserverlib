package com.thinknsync.observerhost;

import java.util.List;

public interface ObserverHost<T> {
    void addToObservers(TypedObserver<T> observer);
    void addToObservers(List<TypedObserver<T>> observers);
    void removeObserver(TypedObserver<T> observer);
    void clearObservers();
    void notifyObservers(T object);
    void notifyError(Exception e);
}
